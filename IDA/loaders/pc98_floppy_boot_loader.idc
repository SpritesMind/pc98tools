#include <idc.idc>
#include <idc98.idc>

/*****
 * PC9801 floppy boot loader
 *
 * Based on 
 * http://fileformats.archiveteam.org/wiki/Anex86_PC98_floppy_image
 * https://github.com/joncampbell123/dosbox-x/blob/820931be10a73a396bed7d067db52adda07f8148/src/dos/dos_programs.cpp#L1163
 *
 * v0.1 : initial version
 * v0.2 : load boot sector only + idc98.idc 
 *****/

//li - loader_input_t object
//n  - how many times we have been called
static accept_file(li, n) {
  auto fileext;
  if (n)	return 0;
  
  //TODO : add d88 support
  
  fileext = GetInputFile();
  fileext = substr(fileext, strstr(fileext, ".")+1, -1);
  if ( (strlen(fileext)==3) && ((strstr(fileext, "fdi") == 0) || (strstr(fileext, "FDI") == 0)) )
  {
	return "PC98 Floppy boot loader";
  }
  
  return 0;
}

// Load the file into the database
//      li      - loader_input_t object. it is positioned at the file start
//      neflags - combination of NEF_... bits describing how to load the file
//                probably NEF_MAN is the most interesting flag that can
//                be used to select manual loading
//      format  - description of the file format
// Returns: 1 - means success, 0 - failure
static load_file(li, neflags, format) {
	auto fddType, headerSize, fddSize, sectorSize, sectorsCount, surface, cylinders;
	auto bootSectors;
	auto base;
	
	SetPrcsr("80286r");

	li.seek(4, SEEK_SET);
	li.readbytes(&fddType, 4, 0);
	li.readbytes(&headerSize, 4, 0);
	li.readbytes(&fddSize, 4, 0);
	li.readbytes(&sectorSize, 4, 0);
	li.readbytes(&sectorsCount, 4, 0);
	li.readbytes(&surface, 4, 0);
	li.readbytes(&cylinders, 4, 0);

	
    if (surface == 1)
	{
		bootSectors = 512/sectorSize;
	}
	else
	{
		bootSectors = 1024/sectorSize;
	}
	
   //TODO handle strange floppy with sector 0 size different of others setors
	base = 0x2000 - (sectorSize/16);
    base = base*16;

	//TODO load all floppy or only sector0/1 (number of boot sector is related to floppy configuration)
	li.seek(0, SEEK_SET);
	if (AskYN(0,"Load no-boot sector ?") == 1)  // -1:cancel,0-no,1-ok
	{
		loadfile(li, 0x1000, base, li.size() - 0x1000);
		SegCreate(base + bootSectors*sectorSize, base + fddSize,base/16,0,1,2);
		SegRename(base + bootSectors*sectorSize,"Sectors");
		SegClass (base + bootSectors*sectorSize,"DATA");
		
		SetSegmentType(base + bootSectors*sectorSize,SEG_DATA);
	}
	else
	{
		loadfile(li, 0x1000, base, bootSectors*sectorSize);
	}
	
	
	ExtLinA		(base,	0,	";");
	ExtLinA		(base,	1,	"; FDI header");
	ExtLinA		(base,	2,	form("; - FDD Type          %d", fddType));
	ExtLinA		(base,	3,	form("; - FDD Size          %d", fddSize));
	ExtLinA		(base,	4,	form("; - Sector Size       %d", sectorSize));
	ExtLinA		(base,	5,	form("; - Number of sectors %d", sectorsCount));
	ExtLinA		(base,	6,	form("; - Surface           %d", surface));
	ExtLinA		(base,	7,	form("; - Cylinders         %d", cylinders));
	ExtLinA		(base,	8,	form("; - Boot sectors      %d", bootSectors));
	ExtLinA		(base,	9,	";");
	ExtLinA		(base, 10,	form("; Loaded at %04x:0000", base/16));
	
	SegCreate(base,base + bootSectors*sectorSize,base/16,0,1,2);
	SegRename(base,"BootSector");
	SegClass (base,"CODE");
	
	SegDefReg(base,"es",0);
	SegDefReg(base,"ss",0x30);
	SegDefReg(base,"ds",0);
	SegDefReg(base,"fs",0);
	SegDefReg(base,"gs",0);
	
	SetSegmentType(base,SEG_CODE);

	AddEntryPoint(base,base,"_start",1);
	Wait();
	
	PC98_setup();
	Jump(base);
	

	return 1;
}
