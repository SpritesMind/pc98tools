#include <idc.idc>


// Plugin to fix bad zero terminated string detection of IDA
// IDA bugs on, at least, SJIS strings and bread string earlier when it meets some special values (not zero)
//
// May be not needed on 6.4 version, which introduced (with Alt-A) character encoding recognition

class sjis_plugin_t
{
    sjis_plugin_t( )
	{
		this.flags = 0;
		this.comment = "Make SJIS zero terminated string on cursor";
		this.help = "";
		this.wanted_name = "SJIS String",
		this.wanted_hotkey  = "Alt-Ctrl-S";
	}
	
	init ()
	{
		//Message("%s : init\n", this.wanted_name);
		return PLUGIN_OK;
	}
	
	run(arg)
	{
		auto addr, fileAddr , maxAddr, endAddr;
		auto byte, size, saveStringType;
		saveStringType = GetLongPrm(INF_STRTYPE);
		addr = ScreenEA();
		fileAddr = addr - GetSegmentAttr(addr, SEGATTR_START);
		maxAddr = GetSegmentAttr(addr, SEGATTR_END);
		//Message("%s : run %d at 0x%05x %s (0x%05x)\n", this.wanted_name, arg, addr, atoa(addr), fileAddr);
		
		endAddr = addr;
		while(endAddr <= maxAddr)	
		{
			byte = GetOriginalByte(endAddr);
			if (byte == 0)
			{
				size = endAddr - addr;
				Message("%d characters long string at 0x%05x\n", size, addr);
				//
				SetLongPrm (INF_STRTYPE,ASCSTR_C);
				MakeStr(addr,endAddr+1);
				SetLongPrm (INF_STRTYPE,saveStringType);
				return;
			}
			endAddr++;
			if (endAddr == maxAddr) break;
		}
		Warn("No string at 0x%05x\n", addr);
	}

	term()
	{
		//Message("%s : term\n", this.wanted_name);
	}	
}

static PLUGIN_ENTRY()
{
	return sjis_plugin_t();
}	
