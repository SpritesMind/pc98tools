#include <idc.idc>


// Plugin to export sjis string, since IDA is unable to copy it
//
// May be not needed on 6.4 version, which introduced (with Alt-A) character encoding recognition

class export_sjis_plugin_t
{
    export_sjis_plugin_t( )
	{
		this.flags = 0;
		this.comment = "Export SJIS  string on cursor";
		this.help = "";
		this.wanted_name = "SJIS Export",
		this.wanted_hotkey  = "Alt-Ctrl-X";
	}
	
	init ()
	{
		//Message("%s : init\n", this.wanted_name);
		return PLUGIN_OK;
	}
	
	run(arg)
	{
		auto addr,handle, endAddr, maxAddr;
		auto byte, size,err, filename;
		
		addr = ScreenEA();
		endAddr = addr;
		maxAddr = GetSegmentAttr(addr, SEGATTR_END);

		while(endAddr <= maxAddr)	
		{
			byte = GetOriginalByte(endAddr);
			if (byte == 0)
			{
				filename = "sjis.txt";
				handle = fopen(filename,"w");
				if (handle == -1)
				{
					Message("Can't create file %s\n", filename);
					return;
				}

				size = endAddr - addr;
				while(addr < endAddr)	
				{
					byte = GetOriginalByte(addr);
					err = fputc(byte, handle);
					if (err == -1)
					{
						Message("Can't write to file %s\n", filename);
						fclose(handle);
						return;
					}
					Message("0x%02x at 0x%05x\n", byte, addr);
					addr++;
				}
				fclose(handle);
				Message("%d characters long string at 0x%05x\n", size, ScreenEA());

				return;
			}
			endAddr++;
			if (endAddr == maxAddr) break;
		}
		Warn("No string at 0x%05x\n", addr);
	}

	term()
	{
		//Message("%s : term\n", this.wanted_name);
	}	
}

static PLUGIN_ENTRY()
{
	return export_sjis_plugin_t();
}	
