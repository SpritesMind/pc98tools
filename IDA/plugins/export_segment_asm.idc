#include <idc.idc>


// Plugin to generate ASM from current segment only


class export_asm_t
{
    export_asm_t( )
	{
		this.flags = 0;
		this.comment = "Export current segment to ASM";
		this.help = "";
		this.wanted_name = "ASM Export",
		this.wanted_hotkey  = "Alt-Ctrl-A";
	}
	
	init ()
	{
		//Message("%s : init\n", this.wanted_name);
		return PLUGIN_OK;
	}
	
	run(arg)
	{
		auto addr, startAddr, endAddr, maxAddr;
		auto source, filename,segName;
		
		source = GetInputFile();
		source = substr(source, 0, strstr(source, "."));
  
  
		addr = ScreenEA();
		segName = SegName(addr);
		startAddr = GetSegmentAttr(addr, SEGATTR_START);
		maxAddr = GetSegmentAttr(addr, SEGATTR_END);
		filename =  form("%s_%s.asm", source, segName);

		if ( GenerateFile(OFILE_ASM,fopen(filename,"w"), startAddr, maxAddr, GENFLG_ASMTYPE) == -1 )
		{
			Warning(form("Error : unable to export segment %s to %s\n",segName, filename));
			return;
		}
		else
		{
			Message(form("Segment %s exported to %s\n",segName, filename));
		}
	}

	term()
	{
		//Message("%s : term\n", this.wanted_name);
	}	
}

static PLUGIN_ENTRY()
{
	return export_asm_t();
}	
