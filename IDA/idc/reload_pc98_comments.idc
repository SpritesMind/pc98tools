#include <idc.idc>


// This script for reload of default IDA comments for in/out/int on x86
// useful when you're writing new ida.int/ida64.int with LoadINT


static main()
{
  auto ea;
  for ( ea=MinEA(); ea != BADADDR; ea=NextHead(ea, BADADDR) )
  {
    if ( GetMnem(ea) == "int" )
	{
      Message( "interrupt at 0x%X\n", ea);
	  MakeUnkn(ea, DOUNK_SIMPLE);
	  MakeCode(ea);
	}
	else if ( GetMnem(ea) == "in" )
	{
      Message( "in at 0x%X\n", ea);
	  MakeUnkn(ea, DOUNK_SIMPLE);
	  MakeCode(ea);
	}
	else if ( GetMnem(ea) == "out" )
	{
      Message( "in at 0x%X\n", ea);
	  MakeUnkn(ea, DOUNK_SIMPLE);
	  MakeCode(ea);
	}
  }
}