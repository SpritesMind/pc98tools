#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <argtable3.h>

#include "utils/file_utils.h"
#include "utils/common.h"

#define DEBUG	0

static struct arg_lit *help, *verb;
static struct arg_file *file, *rules;
static struct arg_end *end;
/*
#define EMPTY_CHAR " \t\n\r"

char* copy( char* dst, const char* src ) {
    char* cur = dst;
    while( (*cur++ = *src++) )
        ;
    return dst;
}

char* rtrim( char* str, const char* t )
{
    char* curEnd = str, *end = str;

    char look[ 256 ] = {  1, 0 };
    while( *t )
       look[ (unsigned char)*t++ ] = 1;

    while( *end ) {
      if ( !look[ (unsigned char)*end ] )
          curEnd = end + 1;
      ++end;
    }
    *curEnd = '\0';

    return str;
}

char* ltrim( char* str, const char* t ) {
    char* curStr = NULL;

    char look[ 256 ] = { 1, 0 };
    while( *t )
        look[ (unsigned char)*t++ ] = 1;

    curStr = str;
    while( *curStr && look[ (unsigned char)*curStr ] )
        ++curStr;

    return copy( str, curStr );
}

char* trim( char* str, const char* t ) {
    return ltrim( rtrim( str, t ), t );
}
*/
int inject(int argc, char *argv[])
{	
	int exitcode = 0;
	int err;
	FILE *target = NULL;
	FILE *inr = NULL;
	long maxoffset;

	//line stuff
	char * line = NULL;
    size_t len = 0;
    ssize_t read;

	//parsing stuff
	int linecount;
	int textstart, textend, flags;

	//inject stuff
	unsigned char isInjecting;
	unsigned int injectedChars;
	unsigned int maxChars;


	
	void *argtable[] = {
        help   = arg_litn(NULL, "help", 0, 1, "display this help"),
        rules    = arg_file1("i", "input", "<strings>","strings file"),
        file    = arg_file1("t", "target", "<target>","file to inject strings into"),
		verb    = arg_litn("v", "verbose", 0, 1, "verbose"),
        end    = arg_end(ERROR_BUFFER_SIZE),
    };
	if (arg_nullcheck(argtable) != 0)
	{
		printf("error: insufficient memory\n");
		exitcode = -1;
		goto exit;
    }

	err = arg_parse(argc,argv,argtable);
	
    // special case: '--help' takes precedence over error reporting
    if (help->count > 0)
    {
        printf("Usage:\nINJECT");
        arg_print_syntax(stdout, argtable, "\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		
		exitcode = 0;
		goto exit;
    }
    
	
	// If the parser returned any errors then display them and exit
    if ((err > 0) || (argc == 1))
    {
        // print upto ERROR_BUFFER_SIZE error details contained in the arg_end struct
        arg_print_errors(stdout, end, "error");
        printf("Try 'INJECT --help' for more information.\n");
		exitcode = -1;
		goto exit;
    }

	target = fopen(file->filename[0],"rb+");
	if (!target)
	{
		printf("error : Invalid input filename %s.\n",file->filename[0]);
		exitcode = -1;
		goto exit;
	}
	inr = fopen(rules->filename[0],"rt");
	if (!inr)
	{
		printf("error : Invalid input filename %s.\n",rules->filename[0]);
		exitcode = -1;
		goto exit;
	}

	fseek(target, 0L, SEEK_END);
    maxoffset = ftell(target);

	exitcode = -2;
	linecount = 0;
	isInjecting = FALSE;
	while ((read = getline(&line, &len, inr)) != -1) {
		linecount++;
		//trim(line, EMPTY_CHAR);

		if (strncmp(line, ";", 1) == 0)
		{
			if (isInjecting)
			{
				while(injectedChars < maxChars)
				{
					//0 fill
					fputc(0, target);
					injectedChars++;
				}
				fputc(0, target);
			}
			isInjecting = FALSE;
			free(line);
			line = NULL; //to force getLine to reallocate
			continue; //skip comment
		}	
		else if (strncmp(line, "/", 1) == 0)
		{
			if (isInjecting)
			{
				while(injectedChars < maxChars)
				{
					//0 fill
					fputc(0, target);
					injectedChars++;
				}
				fputc(0, target);
			}
			isInjecting = FALSE;
			free(line);
			line = NULL; //to force getLine to reallocate
			continue;
		}
		else if (strncmp(line, "#", 1) == 0) 
		{
			if (isInjecting)
			{
				while(injectedChars < maxChars)
				{
					//0 fill
					fputc(0, target);
					injectedChars++;
				}
				fputc(0, target);
			}
			textstart = textend = flags = 0; //default values if not found
			sscanf(line,"#%x:%x:%x", &textstart, &textend,&flags);

			if ( (textend < textstart) || (textend == textstart)){
				printf("Error : invalid offsets at line %d\n", linecount-1);
				goto exit;
			}
			if (textend > maxoffset){
				printf("Error : invalid end offset 0x%x at line %d\n",textend, linecount-1);
				goto exit;
			}
			if (textstart >= maxoffset){
				printf("Error : invalid start offset 0x%x at line %d\n",textstart, linecount-1);
				goto exit;
			}
			if (fseek(target, textstart, SEEK_SET) != 0)
			{
				//should not happen because of previous test
				printf("Error : invalid offset 0x%x at line %d\n",textstart, linecount-1);
				goto exit;
			}

			isInjecting = TRUE;
			injectedChars = 0;
			maxChars = textend-textstart-1; //0 ending  
			if ((verb->count > 0))	printf("Inject text to 0x%x (0x%x)\n", textstart,flags);
			continue;
		}
		else if (isInjecting == FALSE)
		{
			printf("Error : invalid data at line %d\n", linecount-1);	
		}
		else //isInjecting == TRUE
		{
			if (strlen(line) > 0)
			{
				//remove \n 0x0A added by EXTRACT
				if (line[strlen(line)-1] == 0x0A) line[strlen(line)-1] = 0;
			}

			if ((injectedChars + strlen(line)) > maxChars)
			{
				//crop
				fwrite(line, maxChars-injectedChars, 1, target);
				injectedChars = maxChars;
				printf("Warning : text cropped at line %d (%ld vs %d)\n", linecount-1, strlen(line), maxChars-injectedChars);	
			}
			else
			{
				fwrite(line, strlen(line), 1, target);
				injectedChars += strlen(line);
			}
			free(line);
			line = NULL;//to force getLine to reallocate
		}
    }



	exitcode = 0;
	
exit:
	//line isn't freed on error
	if (line != NULL)	free(line);

		
	if (target != NULL)		fclose(target);
	if (inr != NULL)	fclose(inr);
	
    // deallocate each non-null entry in argtable[]
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    return exitcode;
}
