#include <stdio.h>
#include <strings.h>
#include <argtable3.h>

#define ERROR_BUFFER_SIZE 4

extern int extract(int argc, char *argv[]);
extern int inject(int argc, char *argv[]);

struct arg_lit *help, *version;
struct arg_str *command;
struct arg_end *end;


static void printver()
{
	printf("PC98 Translate helper v0.1\n");
}

int main(int argc, char *argv[])
{
	int exitcode = 0;
	int err;

	void *argtable[] = {
        version = arg_litn(NULL, "version", 0, 1, "display version info"),
        help    = arg_litn(NULL, "help", 0, 1, "display this help"),
		command = arg_str1(NULL,NULL, "<command> [options]", "execute command" ),
        end     = arg_end(ERROR_BUFFER_SIZE),
    };
	if (arg_nullcheck(argtable) != 0)
	{
		printf("error: insufficient memory\n");
		exitcode = -1;
		goto exit;
    }
    
    err = arg_parse(argc,argv,argtable);
	if (command->count == 1)
	{
		if (strcmp("EXTRACT", command->sval[0]) == 0)
		{
			exitcode = extract(argc-1, argv+1);
			goto exit;
		}
		else if (strcmp("INJECT", command->sval[0]) == 0)
		{
			exitcode = inject(argc-1, argv+1);
			goto exit;
		}
		
	}
    // special case: '--help' takes precedence over error reporting
    if (help->count > 0)
    {
		printver();
        printf("Usage:\ntranshelper");
        arg_print_syntax(stdout, argtable, "\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		 printf("  <command> --help          display command help\n");
		
        printf("\nValid commands:\n");
        printf("  EXTRACT\textract texts from a binary file\n");
        printf("  INJECT\tinject texts to a binary file\n");
		exitcode = 0;
		goto exit;
    }
	
	// special case 2
	if (version->count)
	{
		printver();
		exitcode = 0;
		goto exit;
	}
	
	// If the parser returned any errors then display them and exit
    if ((err > 0) || (argc == 1))
    {
        // print upto ERROR_BUFFER_SIZE error details contained in the arg_end struct
        arg_print_errors(stdout, end, "transhelper");
        printf("Try '--help' for more information.\n");
		exitcode = -1;
		goto exit;
    }

exit:
    // deallocate each non-null entry in argtable[]
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    return exitcode;
}
