#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <argtable3.h>

#include "utils/file_utils.h"
#include "utils/common.h"

#define DEBUG	0

static struct arg_lit *help, *verb;
static struct arg_file *file, *rules, *output;
static struct arg_end *end;
/*
#define EMPTY_CHAR " \t\n\r"

char* copy( char* dst, const char* src ) {
    char* cur = dst;
    while( (*cur++ = *src++) )
        ;
    return dst;
}

char* rtrim( char* str, const char* t )
{
    char* curEnd = str, *end = str;

    char look[ 256 ] = {  1, 0 };
    while( *t )
       look[ (unsigned char)*t++ ] = 1;

    while( *end ) {
      if ( !look[ (unsigned char)*end ] )
          curEnd = end + 1;
      ++end;
    }
    *curEnd = '\0';

    return str;
}

char* ltrim( char* str, const char* t ) {
    char* curStr = NULL;

    char look[ 256 ] = { 1, 0 };
    while( *t )
        look[ (unsigned char)*t++ ] = 1;

    curStr = str;
    while( *curStr && look[ (unsigned char)*curStr ] )
        ++curStr;

    return copy( str, curStr );
}

char* trim( char* str, const char* t ) {
    return ltrim( rtrim( str, t ), t );
}
*/
int extract(int argc, char *argv[])
{	
	int exitcode = 0;
	int err;
	char *outputfilename = NULL;
	FILE *in = NULL;
	FILE *inr = NULL;
	FILE *out = NULL;
	long maxoffset;

	//line stuff
	char * line = NULL;
    size_t len = 0;
    ssize_t read;

	//parsing stuff
	int linecount;
	int textstart, textend, flags;

	
	void *argtable[] = {
        help   = arg_litn(NULL, "help", 0, 1, "display this help"),
        file    = arg_file1("i", "input", "<infile>","input file"),
        rules    = arg_file1("r", "rules", "<rules>","rules file"),
        output = arg_file0("o", "output", "<outfile>","output file"),
		verb    = arg_litn("v", "verbose", 0, 1, "verbose"),
        end    = arg_end(ERROR_BUFFER_SIZE),
    };
	if (arg_nullcheck(argtable) != 0)
	{
		printf("error: insufficient memory\n");
		exitcode = -1;
		goto exit;
    }

	err = arg_parse(argc,argv,argtable);
	
    // special case: '--help' takes precedence over error reporting
    if (help->count > 0)
    {
        printf("Usage:\nEXTRACT");
        arg_print_syntax(stdout, argtable, "\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		
		exitcode = 0;
		goto exit;
    }
    
	
	// If the parser returned any errors then display them and exit
    if ((err > 0) || (argc == 1))
    {
        // print upto ERROR_BUFFER_SIZE error details contained in the arg_end struct
        arg_print_errors(stdout, end, "error");
        printf("Try 'EXTRACT --help' for more information.\n");
		exitcode = -1;
		goto exit;
    }

	in = fopen(file->filename[0],"rb");
	if (!in)
	{
		printf("error : Invalid input filename %s.\n",file->filename[0]);
		exitcode = -1;
		goto exit;
	}
	inr = fopen(rules->filename[0],"rt");
	if (!inr)
	{
		printf("error : Invalid input filename %s.\n",rules->filename[0]);
		exitcode = -1;
		goto exit;
	}
	
	outputfilename = (char *)output->filename[0];
	if (output->count == 0)	
	{
		outputfilename = calloc(FILENAME_MAX, 1);
		if (outputfilename == NULL)
		{
			printf("error : Not enough memory (%dbytes).\n",FILENAME_MAX);
			exitcode = -1;
			goto exit;
		}
		
		file_getNewFilename((char *)file->filename[0], outputfilename, ".txt");
	}

	out = fopen(outputfilename,"wt");
	if (!out)
	{
		printf("error : Invalid output filename %s.\n",outputfilename);
		exitcode = -1;
		goto exit;
	}

	fseek(in, 0L, SEEK_END);
    maxoffset = ftell(in);

	exitcode = -2;
	linecount = 0;
	while ((read = getline(&line, &len, inr)) != -1) {
		linecount++;
		//trim(line, EMPTY_CHAR);

		if (strncmp(line, ";", 1) == 0){
			free(line);
			line = NULL; //to force getLine to reallocate
			continue; //skip comment
		}	
		if (strncmp(line, "/", 1) == 0)
		{
			//flush  comment to ouput
			fwrite(line, strlen(line), 1, out);
			free(line);
			line = NULL; //to force getLine to reallocate
			continue;
		}
		if (strncmp(line, "#", 1) != 0) //we have a problem!
		{
			printf("Error: Retrieved an unparsed line at line %d\n", linecount-1);
        	printf("%s\n", line);
			continue;
		}
		textstart = textend = flags = 0; //default values if not found
        sscanf(line,"#%x:%x:%x", &textstart, &textend,&flags);
		
		if ( (textend < textstart) || (textend == textstart)){
			printf("Error : invalid offsets at line %d\n", linecount-1);
			goto exit;
		}
		if (textend > maxoffset){
			printf("Error : invalid end offset 0x%x at line %d\n",textend, linecount-1);
			goto exit;
		}
		if (textstart >= maxoffset){
			printf("Error : invalid start offset 0x%x at line %d\n",textstart, linecount-1);
			goto exit;
		}
		if (fseek(in, textstart, SEEK_SET) != 0)
		{
			//should not happen because of previous test
			printf("Error : invalid offset 0x%x at line %d\n",textstart, linecount-1);
			goto exit;
		}
		
		if ((verb->count > 0))	 printf("Extract text: 0x%x to 0x%x (0x%x)\n", textstart, textend, flags);

		fwrite(line, strlen(line), 1, out);
		free(line);

		line = calloc(textend-textstart,1); //calloc zero init
		fread(line, textend-textstart, 1, in);
		fwrite(line, strlen(line), 1, out); //strlen allows us to skip unneeded 0-end
		free(line);
		fwrite("\n", 1, 1, out);

		line = NULL;//to force getLine to reallocate
    }



	exitcode = 0;
	
exit:
	//line isn't freed on error
	if (line != NULL)	free(line);

	if ((output->count == 0) && (outputfilename != NULL))	{free(outputfilename);outputfilename = NULL;}
		
	if (in != NULL)		fclose(in);
	if (inr != NULL)	fclose(inr);
	if (out != NULL)	fclose(out);

    // deallocate each non-null entry in argtable[]
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    return exitcode;
}
